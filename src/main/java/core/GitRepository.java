package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository {

    private String repoPath;
    private String headPath ="";
    private String refHash = "";

    public GitRepository(){
        repoPath = "";
    }

    public GitRepository(String repoPath){
        this.repoPath = repoPath;
    }

    public String getHeadRef() {
        //return "refs/heads/master";

        headPath = repoPath+"/HEAD";
        try (BufferedReader bf = new BufferedReader(new FileReader(headPath))){
            return bf.readLine().substring(5);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";

    }

    public String getRefHash(String path) {

        /* In directory sample_repos/sample01/refs/heads/ aprire file master e prendere l'hash.
        Devo togliere i primi 2 bit perché indicano la cartella */

        refHash = "sample_repos/sample01/" + path;
        try (BufferedReader bf = new BufferedReader(new FileReader(refHash))) {
            return bf.readLine().substring(2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";

    }

}
