package core;

import sun.misc.IOUtils;

import java.io.*;
import java.util.zip.InflaterInputStream;

public class GitBlobObject {

    private String repoPath;
    private String s;
    private StringBuilder sb = new StringBuilder();


    public GitBlobObject(String repoPath, String s) {

        this.repoPath = repoPath;
        this.s = s;

    }

    public String getType() {

        String destFile = repoPath + "/objects/" + s.substring(0, 2) + "/" + s.substring(2);
        String line;

        try (InputStream is = new InflaterInputStream(new FileInputStream(destFile))) {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
            return sb.toString().substring(0, 4);
            //return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";

    }

    public String getContent() {

        int idx = sb.toString().indexOf("\0");

        String content = sb.toString().substring(idx+1);

        return content;
    }
}
